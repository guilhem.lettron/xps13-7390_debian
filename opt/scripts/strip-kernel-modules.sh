#!/bin/sh

if [ -d "/lib/modules/5.4.0-rc4" ]; then
  cd /lib/modules/5.4.0-rc4
fi

if [ -d "/lib/modules/5.4.0-rc4+" ]; then
  cd /lib/modules/5.4.0-rc4+
fi

sudo find . -name *.ko -exec strip --strip-unneeded {} +
